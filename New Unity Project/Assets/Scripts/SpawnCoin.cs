﻿using UnityEngine;
using System.Collections;

public class SpawnCoin : MonoBehaviour {


	public int coinAmount = 30;
	public Transform[] coinSpawns;
	public GameObject coin;

	// Use this for initialization
	void Start () {

		Spawn();
	}

	void Spawn()
	{
		for (int i = 0; i < coinSpawns.Length; i++)
		{
			int coinFlip = Random.Range (0, coinAmount);
			if (coinFlip > (coinAmount - 2))
				Instantiate(coin, coinSpawns[i].position, coin.transform.rotation);
		}
	}

}