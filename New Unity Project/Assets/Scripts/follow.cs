﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follow : MonoBehaviour {
	public GameObject player;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
		//offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		float newXPosition = player.transform.position.x - offset.x;
		float newYPosition = player.transform.position.y - offset.y;
		float newZPosition = offset.z;

		transform.position = new Vector3(newXPosition, newYPosition, newZPosition);
	}
}
