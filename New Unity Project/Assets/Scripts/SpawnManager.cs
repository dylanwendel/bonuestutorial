﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

	public int maxPlatforms =25;
	public GameObject platform;
	public GameObject finalPlatform;
	private float horizontalMin = 7.5f;
	private float horizontalMax = 14f;
	private float verticalMin = -6f;
	private float verticalMax = 6;


	private Vector2 originPosition;


	void Start () {

		originPosition = transform.position;
		Spawn ();

	}

	void Spawn()
	{
		for (int i = 0; i < maxPlatforms; i++)
		{
			Vector2 randomPosition = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range (verticalMin, verticalMax));
			Instantiate(platform, randomPosition, Quaternion.identity);
			originPosition = randomPosition;
			if (horizontalMax < 20f) {
				horizontalMax = horizontalMax + 1.0f;
				horizontalMin = horizontalMin + 1.0f;
			}

		}

		Vector2 randomPosition2 = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range (verticalMin, verticalMax));
		print ("randPos2: " + randomPosition2);
		Instantiate(finalPlatform, randomPosition2, Quaternion.identity);
	}

}